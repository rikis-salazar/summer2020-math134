# Homework 5

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=3001025

**Due date:** ~~Monday, July 27~~ (extended to Friday, July 31).

* Quiz 7 (Week 5, Thursday afternoon PDT) is based on questions 6--15 **from
  homework assignment 4**.

* Quiz 8 (Week 6, Tuesday afternoon PDT) is based on questions 1--10 from this
  [last] assignment.

---

## Regular exercises

> From Strogatz (_Nonlinear Dynamics and Chaos_).

Section 5.3

1.  Problem 5.3.2 b) c).
1.  Problem 5.3.3.
1.  Problem 5.3.6.

Section 6.1

4.  Problem 6.1.1, 6.1.6.
4.  Problem 6.1.2, 6.1.5.

Section 6.2

6.  Problem 6.2.1.
6.  Problem 6.2.2.

Section 6.3

8.  Problem 6.3.1.
8.  Problem 6.3.2.
8.  Problem 6.3.3.

Section 6.4

10. Problem 6.4.1
10. Problem 6.4.2

Section 6.5

12.  Problem 6.5.1.
12.  Problem 6.5.2.

Section 6.6

14. Problem 6.6.1.
14. Problem 6.6.2.

---

##  Miscellaneous exercises

> From Strogatz (_Nonlinear Dynamics and Chaos_).

Section 5.2

*   Problem 5.2.13.

    > _Hint:_ see Monday (week 5) lecture and pretend there is no gravity force.

Section 6.3

*   Problem 6.3.9 a) -- d).

Section 6.4

*   Problem 6.4.4.
*   Problem 6.4.6.

Section 6.5

*   Problem 6.5.20 a) b) c).

Section 6.6

*   Problem 6.6.6.

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

