# Homework assignments

Homework assignment problems fall into two categories: _regular_, and
_miscellaneous_ exercises.

*   _Regular_ are the basis for the list of 5 problems that will be assigned to
    you every week. As explained in the [course syllabus][syll], this list will
    be randomly generated and likely different for every student. You should
    select 2 problems out of the 5 in the list and make sure your solutions are
    _accurate_, and that your submissions _look good_. For the first two weeks
    you'll be allowed to submit pictures and/or _pdf_ documents to gradescope,
    but eventually you will be asked to _write your arguments_ directly into the
    Gradescope platform.

    > **Note:** The small-size/high-quality, 2-step procedure described in the
    > syllabus does not apply to homework submissions.

*   _Miscellaneous_ exercises are for you to practice key concepts/tools we
    develop during lecture, and/or to help you further explore
    ideas/models/solutions that were briefly discussed during lecture.

    > These problems will not be collected (nor graded), but you are more than
    > welcome to ask me or your TA about them during office hours.

In addition, every homework assignment will be marked with either an _in
progress_, or _final_ status.

*   _In progress (status)_ means that more problems could be added to the
    corresponding assignment. Once the list is complete, the old status will be
    replaced with the _final_ status (see below).
*   _Final (status)_ means that no more problems will be added to the
    corresponding assignment.

---

## Assignment links

*   [Homework 1][h1]: [click this link][list1] to find the random list assigned
    to you for this assignment.

*   [Homework 2][h2]: [click this link][list2] to find the random list assigned
    to you for this assignment.

*   [Homework 3][h3]: [click this link][list3] to find the random list assigned
    to you for this assignment.

*   [Homework 4][h4]: [click this link][list4] to find the random list assigned
    to you for this assignment.

*   [Homework 5][h5]: [click this link][list5] to find the random list assigned
    to you for this assignment.

[h1]: hw1/
[list1]: hw1/random-list.txt
[h2]: hw2/
[list2]: hw2/random-list.txt
[h3]: hw3/
[list3]: https://www.pic.ucla.edu/~rsalazar/easter-egg.mp4
[h4]: hw4/
[list4]: hw4/random-list.txt
[h5]: hw5/
[list5]: hw5/random-list.txt

<!-- One more comment
[list1]: https://www.kualo.co.uk/404
[list2]: https://www.youtube.com/watch?v=yQq1-_ujXrM
[list4]: https://www.kualo.co.uk/404
[list5]: https://www.kualo.co.uk/404
-->

---


[Return to main course website][MAIN]

[syll]: ../syllabus/


[MAIN]: ..


