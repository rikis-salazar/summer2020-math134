# Homework 1

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=3001025

**Due date:** Monday, June 29.

* Quiz 1 (Week 1, Thursday afternoon PDT) is based on questions 1--10.

* Quiz 2 (Week 2, Tuesday afternoon PDT) is based on questions 11--18.

---

## Regular exercises

> From Strogatz (_Nonlinear Dynamics and Chaos_).

Section 2.1

1.  Problems 2.1.1, 2.1.2, and 2.1.3.
1.  Problem 2.1.4 a).

Section 2.2

3.  Problem 2.2.1.
3.  Problem 2.2.3.
3.  Problem 2.2.4.
3.  Problem 2.2.6.
3.  Problem 2.2.7.
3.  Problem 2.2.8.
3.  Problem 2.2.9.
3.  Problem 2.2.10.
11. (Terminal velocity) The velocity $v(t)$ of a skydiver falling to the ground
    is governed by $m\dot{v} = mg - kv$, where $m$ is the mass of the skydiver,
    $g$ is the acceleration due to gravity, and $k > 0$ is a constant related to
    the amount of air resistance.
    i.  Obtain the analytical solution for $v(t)$, assuming that $v(0)=0$.
    i.  Find the limit of $v(t)$ as $t\to\infty$. This limiting velocity is
        called the _terminal velocity_.
    i.  Give a graphical analysis of this problem, and thereby re-derive a
        formula for the terminal velocity.

    > _Notes:_
    >
    > * This problem is a slight variation of 2.2.13. The friction term in this
    >   case is linear; in problem 2.2.13 it is quadratic. Unsurprisingly, the
    >   analytic solution in the quadratic case is a little harder to obtain.
    > * In both of these two problems positive displacements _point_ towards the
    >   floor. In other words, a positive velocity indicates that the skydiver
    >   is falling.

Section 2.3

12. Problem 2.3.1.
12. Problem 2.3.3.

    > _Notes:_
    >
    > -   For part a), interpret only the constant $b$. It might help to try to
    >     interpret instead its reciprocal $b^{-1}$. 
    > -   According to _Wikipedia_, the constant $a$ is _"related to the
    >     proliferative ability of the cells [within the tumor]"_.

12. Problem 2.3.4.

Section 2.4

15. Problems 2.4.1, 2.4.4, 2.4.6.
15. Problems 2.4.2, 2.4.3, 2.4.5.
15. Problems 2.4.7.
15. Problems 2.4.8.

---

##  Miscellaneous exercises

> From Strogatz (_Nonlinear Dynamics and Chaos_).

Section 2.1

*   Problem 2.1.4 b).

Section 2.2

*   Problem 2.2.11.
*   Problem 2.2.13.

Section 2.3

*   Problem 2.3.5.

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

