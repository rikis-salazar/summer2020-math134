# Homework 3

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=3001025

**Due date:** Monday, July 13.

* Quiz 4 (Week 3, Thursday afternoon PDT) is based on questions 10--18 **from
  assignment 2**.

* Quiz 5 (Week 4, Tuesday afternoon PDT) is based on questions 11--16 from this
  assignment.

---

## Regular exercises

> From Strogatz (_Nonlinear Dynamics and Chaos_).

Section 3.5

1.  Problems 3.5.1, 3.5.2.
1.  Problem 3.5.3.

    > _Note:_ Equation (3.5.7) referenced in this problem is
    > \begin{equation}
    >   \frac{d\phi}{d\tau} = \sin\phi \big(\gamma\cos\phi - 1\big).
    > \end{equation}

3.  Problem 3.5.4 b) and c).

    > _Note:_ Take for granted that the solution for part a) is given by the
    > following system
    > $$m\ddot{x} + b\dot{x} + kx\Big( 1 - \frac{L}{\sqrt{h^{2} + x^{2}}} \Big)
    >   =0.$$

4.  Problem 3.5.4 d). 
4.  Problem 3.5.6 a) and b). 

    > _Note:_ The solution to this second order differential equation is a
    > linear combination of the exponential functions
    > \begin{align}
    >  x_{1}(t) & = \mathrm{e}^{r_{1}t}, &
    >  x_{2}(t) & = \mathrm{e}^{r_{2}t}.
    > \end{align}
    > The time scales that you want are given by the absolute values of the
    > reciprocals of $r_{1}$ and $r_{2}$.
    >
    > > I will explain during lecture the basic idea behind this concept.

6.  Problem 3.5.7 a) and b).
6.  Problem 3.5.8.

Section 3.6

8.  Write a brief summary (2 or 3 paragraphs) of section 3.6

    > _Note:_ The goal here is twofold: not only will you be reviewing this
    > section, but also you will get the chance to practice your typesetting
    > skills using $\LaTeX$.

9.  Problem 3.6.1.
9.  Problem 3.6.2 a).

Section 4.1

11. Problem 4.1.1.
11. Problem 4.1.2, 4.1.5.
11. Problem 4.1.3, 4.1.4.
11. Problem 4.1.8 a) b).

Section 4.2

15. Problem 4.2.1.
15. Problem 4.2.3.

---

##  Miscellaneous exercises

> From Strogatz (_Nonlinear Dynamics and Chaos_).

Section 3.5

*   Problem 3.5.4 a.

    > _Note:_ The _answer_ to this problem can be found above. If you want to
    > tackle this, you should come up with a detailed solution.

*   Problem 3.5.5 b) and c). 
*   Problem 3.5.6 c) and d). 

    > _Note:_ Plot the solutions $x_{1}$ and $x_{2}$ described above for several
    > small values of $\epsilon$. _E.g.:_ 0.1, 0.01, etc.

*   Problem 3.5.7 c) and d). 

Section 3.7

*   Problem 3.7.6.

    > _Note:_ Replace AIDS with COVID-19 in part k).

Section 4.1

*   Problem 4.1.9.

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

