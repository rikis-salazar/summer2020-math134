# Homework 2

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=3001025

**Due date:** Monday, July 6.

* Quiz 3 (Week 2, Thursday afternoon PDT) is based on questions 1--9.

* Midterm 1 (Week 3, Tuesday afternoon PDT) will be based on the sections listed
  in assignments 1 & 2.

---

## Regular exercises

> From Strogatz (_Nonlinear Dynamics and Chaos_).

Section 2.5

1.  $\star$ Problem 2.5.1.
1.  Problem 2.5.3.
1.  Problem 2.5.4.

Section 2.6

4.  Problem 2.6.1.
4.  Problem 2.6.2.

    > **Errata:** Please use the integral below instead of the one that
    > appears in your textbook.
    > $$\int_{t}^{t+T} f\big(x(s)\big) \dot{x}(s) \,ds$$

Section 2.7

6.  Problems 2.7.2, 2.7.3.
6.  Problems 2.7.4, 2.7.5.
6.  Problem 2.7.6.
6.  $\star$ Problem 2.7.7.

Section 3.1

10. Problems 3.1.1, 3.1.4.
10. Problems 3.1.2, 3.1.3.

Section 3.2

12. Problems 3.2.1, 3.2.4.
12. Problems 3.2.2, 3.2.3.

Section 3.4

14. Problems 3.4.1, 3.4.4.
14. Problems 3.4.2, 3.4.3.
14. Problems 3.4.5, 3.4.8.
14. Problems 3.4.6, 3.4.7.
14. $\star$ Problem 3.4.14.

---

##  Miscellaneous exercises

> From Strogatz (_Nonlinear Dynamics and Chaos_).

Section 2.4

*   Problem 2.4.9 a.

Section 2.5

*   Problem 2.5.2.
*   Problem 2.5.5.

Section 2.8

*   Problem 2.8.3.

Section 3.4

*   Problem 3.4.11.

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

