# Homework 4

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=3001025

**Due date:** Monday, July 20.

* Midterm 2 (Week 5, Tuesday afternoon PDT) will be based on the sections listed
  in assignments 3 & 4.

* Quiz 6 (Week 4, Thursday afternoon PDT) is based on questions 1--5.

---

## Regular exercises

> From Strogatz (_Nonlinear Dynamics and Chaos_).

Section 4.3

1.  Problems 4.3.3, 4.3.6.
1.  Problems 4.3.4, 4.3.5.

Section 4.4

3.  Problem 4.4.1.
3.  Problem 4.4.4 a) b) c).

Section 4.4

5.  Problem 4.5.1 a) b).

    > According to the textbook (see page 105):
    >
    > _"For a range of periods close to the firefly’s natural period (about 0.9
    > sec), the firefly was able to match its frequency to the periodic
    > stimulus. In this case, one says that the firefly had been **entrained**
    > [emphasis mine] by the stimulus. However, if the stimulus was too fast or
    > too slow, the firefly could not keep up and entrainment was lost..."_

Section 5.1

6.  Problem 5.1.1.
6.  Problem 5.1.2.
6.  Problems 5.1.3, 5.1.4, 5.1.5, 5.1.6.
6.  Problem 5.1.9 a) b) c).
6.  Problem 5.1.10 a) b) e) f).
    

Section 5.2

11. Problem 5.2.1.
11. Problem 5.2.3.
11. Problem 5.2.4.
11. Problem 5.2.7.
11. Problem 5.2.10.

---

##  Miscellaneous exercises

> From Strogatz (_Nonlinear Dynamics and Chaos_).

Section 4.3

*   Problem 4.3.1.
*   Problem 4.3.2.

Section 4.4

*   Problem 4.4.4 d).

Section 5.1

*   Problem 5.1.9 a) b) c).
*   Prove that your answers to problem 10 (above) are correct, using the
    definitions of the different types of stability. 

    > _Note:_
    >
    > You must produce a suitable $\delta$ to prove that the origin is
    > attracting, or a suitable $\delta(\epsilon)$ to prove _Liapunov_
    > stability.

*   Problem 5.2.2.

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

