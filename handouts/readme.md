# Miscellaneous handouts

In this section you will find material that is related to the contents we are
discussing during lecture, however, presenting such material during class will
either take extra time (hence reducing the amount of time spent dedicated to
other topics), or will not add anything new to our general discussion.

It is my hope that at some point you go over the contents of these handouts
without feeling pressured to study them as they are not a good fit for a midterm
nor a final exam.

1.  [_Accessing Zoom meetings (no waiting room)._][h1]
1.  [_More about the stability analysis result presented during lecture._][h2]

---

[Return to main course website][MAIN]


[h1]: ../zoom-no-waiting-room/
[h2]: https://www.youtube.com/watch?v=yQq1-_ujXrM

<!-- This is a comment
[h2]: handout2/
-->

[MAIN]: ..

