# Lecture notes

This is the place to find _pdf_ files of the documents I use during lecture.

---

## Week 6

*   Thursday: Review and examples (no lecture notes).

*   Wednesday: [2-D non linear systems (reversible systems)][w6-l2].

*   Monday: [2-D non linear systems (conservative systems)][w6-l1].

## Week 5

*   Thursday: [2-D non linear systems (phase portraits and examples)][w5-l3].

*   Wednesday: [2-D non linear systems (basic concepts)][w5-l2].

*   Monday: [A spring system (review of some important concepts)][w5-l1].

## Week 4

*   Thursday: [Classification of 2-D linear systems][w4-l3].

*   Wednesday: [2-D flows (definitions and examples)][w4-l2].

*   Monday: [Overdamped pendulum driven by constant torque, fireflies][w4-l1].

## Week 3

*   Thursday: [An insect outbreak, flows on the circle][w3-l3].

*   Wednesday: [Bead in a rotating hoop, imperfect bifurcations &
    catastrophes][w3-l2].

*   Monday: [Laser, free fall, bead in a rotating hoop (incomplete)][w3-l1].

## Week 2

*   Thursday: [Transcritical and pitchfork bifurcations][w2-l3].

    > _Errata:_ On page 74 (Thursday, part 2), I incorrectly wrote that we
    > needed to impose the condition $|f'(0)|<\infty$. The correct assumption
    > should be instead $|h'(0)|<\infty$. This has been reflected in the notes
    > linked here. Also, towards the end of the lecture I keep repeating
    > something along the lines of "4 fixed points". This ignores the other
    > fixed point that we had already analyzed, namely $x_{\star} = 0$.

*   Wednesday: [Impossibility of oscillations, saddle-node
    bifurcations][w2-l2].

*   Monday: [Existence and uniqueness, impossibility of oscillations
    (incomplete), and potentials][w2-l1].

## Week 1

*   Thursday: [Phase portraits, population growth, and stability
    analysis][w1-l3].

    > _Errata:_ On page 21 (Thursday, part 1), I wrote $\frac{\pi}{4}$ at the
    > $\frac{3\pi}{4}$ mark in both, the graph of all trajectories, as well as
    > the phase portrait. This mistake doesn't really alter any of the points I
    > was trying to make.

*   Tuesday: [Intuitive examples and 1-dimensional _flows_][w1-l2].

*   Monday: No class notes. Please make sure to read the [syllabus][syl].

[syl]: ../syllabus/
[w1-l2]: 01-wed.pdf
[w1-l3]: 01-fri.pdf

[w2-l1]: 02-mon.pdf
[w2-l2]: 02-wed.pdf
[w2-l3]: 02-fri.pdf

[w3-l1]: 03-mon.pdf
[w3-l2]: 03-wed.pdf
[w3-l3]: 03-fri.pdf

[w4-l1]: 04-mon.pdf
[w4-l2]: 04-wed.pdf
[w4-l3]: 04-fri.pdf

[w5-l1]: 05-mon.pdf
[w5-l2]: 05-wed.pdf
[w5-l3]: 05-fri.pdf

[w6-l1]: 06-mon.pdf
[w6-l2]: 06-wed.pdf

---

[Return to main course website][MAIN]


[MAIN]: ..

